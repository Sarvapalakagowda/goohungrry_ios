//
//  CartViewControler.swift
//  gooHun
//
//  Created by ashish pandita on 5/7/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import Foundation
import UIKit
class CartViewControler: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet var loader: UIActivityIndicatorView!
    
    @IBOutlet var itemsTable: UITableView!
    var cartItems: [AnyObject] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Swift.debugPrint("Swift: \(self.debugDescription)")
       
        cartItems =  CartDataSource.sharedInstance.checkOutItems
        itemsTable.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        itemsTable.dataSource = self
        itemsTable.delegate = self
        itemsTable.tableFooterView = UIView()
  
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //tableview delegate
    func numberOfSections(in tableView: UITableView) -> Int  {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.itemsTable.dequeueReusableCell(withIdentifier: "cartListCell", for:indexPath as IndexPath) as! cartListCell
       
        let menuModal = cartItems[indexPath.row] as! MenuModal
         cell.itemName.text = menuModal._menuName
         cell.typeImg.image = menuModal._menuImage
         cell.itemCount.text = menuModal._seleteditemsCount
       
        
        return cell
        
    }
    
    
    //Protocol update
    
    func checkOutStaus(items: CartDataSource) {
        
        //get the items from here
        print("checkOutStaus:\(CartDataSource.self)")
    }
    
}

