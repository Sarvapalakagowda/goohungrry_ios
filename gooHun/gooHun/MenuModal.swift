//
//  MenuModal.swift
//  gooHun
//
//  Created by ashish pandita on 5/7/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit

class MenuModal: NSObject {
    
    var _menuImage: UIImage? = nil
    var _vegIcon: UIImage? = nil
    var _menuName: String? = nil
    var _menuDesc: String? = nil
    var _cost: String? = nil
    var _pid:String? = nil
    var _seleteditemsCount:String? = nil
    
}
