//
//  menuCell.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 06/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit

class menuCell: UITableViewCell {
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var vegIcon: UIImageView!
    @IBOutlet var menuName: UILabel!
    @IBOutlet var menuDesc: UILabel!
    @IBOutlet var cost: UILabel!
    var productId:String! = nil
    @IBOutlet var count: UILabel!
    private var score = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
        // Configure the view for the selected state
    }

    
    
    @IBAction func addButton(sender: UIButton) {
        score += 1
        count.text = "\(score)"
        
        let  menuModal = MenuModal()
      menuModal._menuImage =  menuImage.image
      menuModal._vegIcon = vegIcon.image
      menuModal._menuName = menuName.text
      menuModal._menuDesc = menuDesc.text
      menuModal._cost = cost.text
      menuModal._seleteditemsCount = count.text
      menuModal._pid  = productId
     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CART_ITEMS_SELECTED"), object: menuModal)
        
    }
    
    
    @IBAction func subButton(sender: UIButton) {
        if score > 0{
        score -= 1
        count.text = "\(score)"
            
            let  menuModal = MenuModal()
            menuModal._menuImage =  menuImage.image
            menuModal._vegIcon = vegIcon.image
            menuModal._menuName = menuName.text
            menuModal._menuDesc = menuDesc.text
            menuModal._cost = cost.text
            menuModal._seleteditemsCount = count.text
            menuModal._pid  = productId
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CART_ITEMS_DESELECTED"), object: menuModal)
            
        }
        
     
    }
    
}
