//
//  cartListCell.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 07/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit

class cartListCell: UITableViewCell {

    @IBOutlet var typeImg: UIImageView!
    
    @IBOutlet var itemName: UILabel!
    
    @IBOutlet var itemCount: UILabel!
   
 
   
    
    private var count = 0

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addButton(sender: UIButton) {
        count += 1
        itemCount.text = "\(count)"
        
    }
    
    
    @IBAction func subButton(sender: UIButton) {
        if count > 0{
            count -= 1
            itemCount.text = "\(count)"

        }else {
            print("menu item deleted")
        }
    }


}
