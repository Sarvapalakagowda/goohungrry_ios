//
//  homeList.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 05/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit

class homeList: UITableViewCell {

  
    @IBOutlet var hotelImage: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var cusine: UILabel!
    @IBOutlet var rate: UILabel!
    @IBOutlet var duration: UILabel!
    @IBOutlet var rating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
