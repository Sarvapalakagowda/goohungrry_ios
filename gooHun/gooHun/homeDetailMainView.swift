//
//  homeDetailMainView.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 06/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit
import Alamofire

class homeDetailMainView: UIViewController {
    var pageMenu : CAPSPageMenu?
    var passedId = String()
    var catArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var controllerArray : [UIViewController] = []
        let categoryIdArray = catArray.value(forKey: "catgoryId") as! NSArray
        let catagoryNameArray = catArray.value(forKey: "catagoryName") as! NSArray
        for i in 0..<catArray.count{
            let fleetDetailsController:homeDetail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeDetail") as! homeDetail
            fleetDetailsController.title = catagoryNameArray[i] as? String
            let idToPass = categoryIdArray[i] as! String
            fleetDetailsController.passedId = idToPass
            controllerArray.append(fleetDetailsController)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(colorWith(hex:"ffffff",andAlpha:1)),
            .addBottomMenuHairline(false),
            .selectionIndicatorHeight(2),
            .selectionIndicatorColor(colorWith(hex:"8BC34A",andAlpha:1)),
            .menuHeight(45.0),.centerMenuItems(false),
            .enableHorizontalBounce(false),
            .useMenuLikeSegmentedControl(false),.menuItemSeparatorPercentageHeight(0.0),
            .viewBackgroundColor(colorWith(hex:"D4D4D4",andAlpha:1)),.selectedMenuItemLabelColor(colorWith(hex:"8BC34A",andAlpha:1))
        ]
        
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0.0, y:0.0, width:self.view.frame.width, height:self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController( self.pageMenu!)
        self.view.addSubview( self.pageMenu!.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
