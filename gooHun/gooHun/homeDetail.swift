//
//  homeDetail.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 06/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit
import Alamofire


class homeDetail: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    var passedId = String()
    var menuArray = NSArray()
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var refreshControl: UIRefreshControl!

    @IBOutlet var menuTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTable.dataSource = self
        menuTable.delegate = self
        menuTable.tableFooterView = UIView()
        menuTable.estimatedRowHeight = 200
        menuTable.rowHeight = UITableViewAutomaticDimension
        menuTable.tableHeaderView?.frame.size.height = 170
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = colorWith(hex: "8BC34A", andAlpha: 1)
        refreshControl.attributedTitle = NSAttributedString(string: "Loading")
        refreshControl.addTarget(self, action: #selector(ViewController.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            menuTable.refreshControl = refreshControl
        } else {
            menuTable.addSubview(refreshControl)
        }
        self.hotelApi(sender: self)
        
    }
    func refresh(sender:AnyObject) {
        self.hotelApi(sender: self)
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuArray.count
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.menuTable.dequeueReusableCell(withIdentifier: "menuCell", for:indexPath as IndexPath) as! menuCell
        
        let menuDict = menuArray[indexPath.row] as! NSDictionary
        let menu_name = menuDict.object(forKey: "menu_name") as! String
        let type = menuDict.object(forKey: "type") as! String
        let menu_desc = menuDict.object(forKey: "menu_desc") as! String
        let menu_price = menuDict.object(forKey: "menu_price") as! String
        let menu_pid = menuDict.object(forKey: "pid") as! String
        
        cell.menuName.text = String(format: "%@", menu_name)
        
        if Int(type) == 1 {
            cell.vegIcon.image = UIImage(named: "ic_veg")
        }else {
             cell.vegIcon.image = UIImage(named: "ic_non_veg")
        }
        
         cell.menuDesc.text = String(format: "%@", menu_desc)
         cell.cost.text = String(format: "₹ %@", menu_price)
         cell.productId =  menu_pid
         cell.count.text = nil
        return cell
        
    }


    
func hotelApi(sender: Any) {
        self.loader.isHidden = false
        let mainUrl = String (format: "%@menuItems",Constants.baseUrl)
        
        //EnginetypeId  //engineIdPassed
        let params:Parameters = [
            "categoryId": passedId,
            ]
        alamoPublicClass.Manager.request(mainUrl, method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("hotelApi response.response \(String(describing: response.response))")// HTTP URL response
                print("hotelApi response.result \(response.result)")// result of response serialization
                if response.result.value == nil {
                    self.loader.isHidden = true
              self.refreshControl.endRefreshing()
                    print("failure")
                    
                }else {
                    if let JSON = response.result.value {
                        print("JSON hotelApi: \(JSON)")
                        var apiResponse = NSArray()
                     apiResponse = JSON as! NSArray
                        print("JSON hotelApi Response: \(apiResponse)")
                       
                            self.menuArray = apiResponse
                            
                            if self.menuArray.count > 0{
                                                                self.menuTable.reloadData()
                            }else {
                                print("bannersList is empty")
                            }
                        self.loader.isHidden = true
                       self.refreshControl.endRefreshing()
                        
                    }
                }
        }
    }
    
    
    
}
