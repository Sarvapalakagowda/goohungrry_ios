//
//  alamoPublicClass.swift
//  Acumen
//
//  Created by Ruthwick S Rai on 06/01/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit
import Alamofire

class alamoPublicClass: NSObject {
    public static var Manager: Alamofire.SessionManager = {
       
        let manager = Alamofire.SessionManager(

        )
        
        return manager
    }()
}


public func colorWith (hex:String, andAlpha:CGFloat) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    var alpha:CGFloat = 0.0
    alpha = andAlpha
    if andAlpha > 1 {
        alpha = 1.0
    }
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: alpha
    )
}

public func setSizeBasedOnscreen (xaxis:CGFloat, yaxis:CGFloat, width:CGFloat, height: CGFloat) -> (CGRect){
    let screenRect: CGRect = UIScreen.main.bounds
    let screenWidth = screenRect.width;
    let screenHeight = screenRect.height;
    
    return CGRect(x:((xaxis*screenWidth)/375), y:((yaxis*screenHeight)/667), width:((width*screenWidth)/375) , height:((height*screenHeight)/667))
}

public func countLabelLines(label: UILabel) -> Int {
    // Call self.layoutIfNeeded() if your view is uses auto layout
    let myText = label.text! as NSString
    
    let attributes = [NSFontAttributeName : label.font]
    let labelSize = myText.boundingRect(with: CGSize(width:label.bounds.width,height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
    
    return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
}


public func changeX (xaxis:CGFloat) -> (CGFloat){
    let screenRect: CGRect = UIScreen.main.bounds
    let screenWidth = screenRect.width;
 
    return CGFloat(((xaxis*screenWidth)/375))
}

public func changeY (yaxis:CGFloat) -> (CGFloat){
    let screenRect: CGRect = UIScreen.main.bounds
   
    let screenHeight = screenRect.height;
    return CGFloat(((yaxis*screenHeight)/667))
}


public func iphoneFive() ->(Bool){
    let screenRect: CGRect = UIScreen.main.bounds
    let screenWidth = screenRect.width;
    if screenWidth == 320 {
        return true
    }else {
        return false
    }
}

public func iphoneSixPlus() ->(Bool){
    let screenRect: CGRect = UIScreen.main.bounds
    let screenWidth = screenRect.width;
    if screenWidth == 414 {
        return true
    }else {
        return false
    }
    
}


public func checkErrorState(errorCode:NSString) ->(Int){
    var errorState = Int()
   
    switch errorCode.intValue {
    case 0:
         errorState = 0
        break
    case 1:
        errorState = 1
        break
    case 2:
        errorState = 2
        break
    case 5:
        errorState = 5
        break
    default:
          errorState = 3
    }
    
   
    return errorState
}




