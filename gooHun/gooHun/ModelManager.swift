//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("listData.sqlite"))
        }
        return sharedInstance
    }
    
    func addUserData(_ menuInfo: menuInfo) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO dataStored (menuId, cost, count, hotelId) VALUES (?, ?, ?, ?)", withArgumentsIn: [menuInfo.menuItemId,menuInfo.cost, menuInfo.count, menuInfo.hotelId])
        sharedInstance.database!.close()
  
        return isInserted
    }
   
    func updateUserData(_ menuInfo: menuInfo) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE dataStored SET menuId=?, cost=?, count=? WHERE hotelId=?", withArgumentsIn: [menuInfo.menuItemId,menuInfo.cost, menuInfo.count, menuInfo.hotelId])
        sharedInstance.database!.close()
        return isUpdated
    }

  

    
    
    func deleteUserData(_ menuInfo: menuInfo) -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM dataStored WHERE hotelId=?", withArgumentsIn: [menuInfo.hotelId])
        sharedInstance.database!.close()
        return isDeleted
    }

    func getAllUserData() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM dataStored", withArgumentsIn: nil)
        let userDetailInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let fullInfo : menuInfo = menuInfo()
                fullInfo.menuItemId = resultSet.string(forColumn: "menuId")
                fullInfo.cost = resultSet.string(forColumn: "cost")
                fullInfo.count = resultSet.string(forColumn: "count")
                fullInfo.hotelId = resultSet.string(forColumn: "hotelId")
                userDetailInfo.add(fullInfo)
            }
        }
        sharedInstance.database!.close()
        print("getAllUserData: \(userDetailInfo)")
        return userDetailInfo
    }
}
