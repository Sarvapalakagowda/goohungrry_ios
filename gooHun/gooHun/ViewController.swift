//
//  ViewController.swift
//  gooHun
//
//  Created by Ruthwick S Rai on 05/05/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var pageControl: UIPageControl!

    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var hotelTable: UITableView!
    
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var refreshControl: UIRefreshControl!
    var bannersList = NSArray()
    var restList = NSArray()
    var passingHotelId = String()
     var passingcatArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        hotelTable.dataSource = self
        hotelTable.delegate = self
        hotelTable.tableFooterView = UIView()
        hotelTable.estimatedRowHeight = 200
        hotelTable.rowHeight = UITableViewAutomaticDimension
         hotelTable.tableHeaderView?.frame.size.height = 170
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = colorWith(hex: "8BC34A", andAlpha: 1)
        refreshControl.attributedTitle = NSAttributedString(string: "Loading")
      refreshControl.addTarget(self, action: #selector(ViewController.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            hotelTable.refreshControl = refreshControl
        } else {
            hotelTable.addSubview(refreshControl)
        }
        self.hotelApi(sender: self)
        
         _ = CartDataSource.sharedInstance
    }
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.hotelApi(sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // collectionView delegate and data source
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bannerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath)  as! bannerCell
        let imageDict = bannersList[indexPath.item] as! NSDictionary
        let imageUrl = imageDict.object(forKey: "bgurl") as! String
        bannerCell.bannerImage.sd_setImage(with: URL(string:imageUrl), placeholderImage: UIImage(named: "location"))
        
        let kCellsPerRow = 1
        let flowLayout:UICollectionViewFlowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let availableWidthForCells: CGFloat =
            self.collectionView.frame.width - flowLayout.sectionInset.left -
                flowLayout.sectionInset.right -
                flowLayout.minimumInteritemSpacing * CGFloat(Float((kCellsPerRow - 1)));
        let  cellWidth: CGFloat = (availableWidthForCells - 10) / CGFloat(Float(kCellsPerRow))
        flowLayout.itemSize = CGSize(width:cellWidth,height: flowLayout.itemSize.height)
        return bannerCell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = collectionView.indexPathForItem(at: center) {
            self.pageControl.currentPage = ip.row
        }
    }    
   
    
    //tableview delegate
    func numberOfSections(in tableView: UITableView) -> Int  {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restList.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.hotelTable.dequeueReusableCell(withIdentifier: "homeList", for:indexPath as IndexPath) as! homeList
        let hotelDict = restList[indexPath.item] as! NSDictionary
        let imageUrl = hotelDict.object(forKey:"image") as! String
        let name = hotelDict.object(forKey:"name") as! String
        let cusineArray = hotelDict.object(forKey:"cuisine") as! NSArray
        let cusine = cusineArray.componentsJoined(by: ",")
        let deliveryTime = hotelDict.object(forKey:"deliverytime") as! String
        cell.name.text = String(format: "%@", name)
        cell.cusine.text = String(format: "%@", cusine)
        cell.duration.text = String(format: "%@", deliveryTime)
        cell.rate.text = "₹₹₹"
        cell.hotelImage.sd_setImage(with: URL(string:imageUrl), placeholderImage: UIImage(named:"placeHolder"))
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          _ = CartDataSource.sharedInstance.checkOutItems.removeAll()
        let hotelDict = restList[indexPath.item] as! NSDictionary
        passingcatArray = hotelDict.object(forKey:"catogory") as! NSArray
       
        passingHotelId = hotelDict.object(forKey:"uuid") as! String

        self.performSegue(withIdentifier: "detail", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detail") {
            let detailController = segue.destination as! homeDetailMainView
            detailController.passedId = passingHotelId
            detailController.catArray = passingcatArray
        }
    }

    
    func hotelApi(sender: Any) {
       
        self.loader.isHidden = false
        let mainUrl = String (format: "%@ioslist",Constants.baseUrl)
         print("hotelApi: ",mainUrl)
        //EnginetypeId  //engineIdPassed
        alamoPublicClass.Manager.request(mainUrl, method: .post, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                print("hotelApi response.response \(String(describing: response.response))")// HTTP URL response
            print("hotelApi response.result \(response.result)")// result of response serialization
            if response.result.value == nil {
                self.refreshControl.endRefreshing()
                self.loader.isHidden = true
                print("failure")
               
            }else {
                if let JSON = response.result.value {
                    print("JSON hotelApi: \(JSON)")
                    var apiResponse = NSDictionary()
                    apiResponse = JSON as! NSDictionary
                    print("JSON hotelApi Response: \(apiResponse)")
                    let errorCode = apiResponse.object(forKey: "statuscode") as! Int
                    if  errorCode == 0{
                    self.bannersList = apiResponse.object(forKey:"banners") as! NSArray
                    self.restList = apiResponse.object(forKey:"rest_list") as! NSArray
                        if self.restList.count > 0{
                        self.hotelTable.reloadData()
                        }else {
                        print("restList is empty")
                        }
                        if self.bannersList.count > 0{
                    self.pageControl.isHidden = false
                    self.pageControl.numberOfPages = self.bannersList.count
                            
                            self.collectionView.reloadData()
                        }else {
                            print("bannersList is empty")
                        }
                        self.refreshControl.endRefreshing()
                          self.loader.isHidden = true
                    }else {
                        self.refreshControl.endRefreshing()
                          self.loader.isHidden = true
                        print("failure")
                        
                    }
                }
            }
        }
    }

    
}

