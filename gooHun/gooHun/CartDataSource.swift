//
//  CartDataSource.swift
//  gooHun
//
//  Created by ashish pandita on 5/7/17.
//  Copyright © 2017 Ruthwick S Rai. All rights reserved.
//

import Foundation
import UIKit


protocol CartDataSourceSubscribe {
    
    func checkOutStaus(items: CartDataSource)
}

final class CartDataSource {
    
    // reachable from other classes
    static let sharedInstance: CartDataSource = CartDataSource()
    
    // properties
    var checkOutItems : [AnyObject] = []
    var delegate: CartDataSourceSubscribe?
    // not reachable from other classes
    private init() {
    
        NotificationCenter.default.addObserver(self, selector: #selector(menuSelected(notification:)), name: NSNotification.Name(rawValue: "CART_ITEMS_SELECTED"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(menuDeSelected(notification:)), name: NSNotification.Name(rawValue: "CART_ITEMS_DESELECTED"), object: nil)
    
    }
    
    @objc func menuSelected(notification: NSNotification) {
        
        let menuModal:MenuModal = notification.object as!MenuModal
        Swift.debugPrint(menuModal._seleteditemsCount!)
        
        // check items in checkOutItems and comapre with menuModal and see if you want to retian them in Array.
       
        
        for (index, element) in checkOutItems.enumerated() {
            print("Item \(index): \(element)")
            let menu : MenuModal = element as! MenuModal
            if(menu._pid == menuModal._pid){
            
                checkOutItems.remove(at: index)
                
            }
            
        }
        
         checkOutItems.append(menuModal)
       
        
        //update any class who wants data from CartDataSourceSubscribe
        delegate?.checkOutStaus(items: self)
      
    }
  
    @objc func menuDeSelected(notification: NSNotification) {
        
        let menuModal:MenuModal = notification.object as!MenuModal
        Swift.debugPrint(menuModal._seleteditemsCount!)
        
        // check items in checkOutItems and comapre with menuModal and see waht you to remove
    
    
        if menuModal._seleteditemsCount == "0" {
            
            for (index, element) in checkOutItems.enumerated() {
                print("Item \(index): \(element)")
                let menu : MenuModal = element as! MenuModal
                if(menu._pid == menuModal._pid){
                    
                    checkOutItems.remove(at: index)
                    
                }
                
            }
        }else{
        
        
            for (index, element) in checkOutItems.enumerated() {
                print("Item \(index): \(element)")
                let menu : MenuModal = element as! MenuModal
                if(menu._pid == menuModal._pid){
                    
                    checkOutItems.remove(at: index)
                    
                }
                
            }
            
            checkOutItems.append(menuModal)
            
        }
        
     
        
         delegate?.checkOutStaus(items: self)
        
    }


    
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "CART_ITEMS_SELECTED"), object: nil);
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "CART_ITEMS_DESELECTED"), object: nil);
        
    }
    
}




